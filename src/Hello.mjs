export default class Hello {

	constructor(name) {
		this.message = `Hello ${name}!`;
	}
  
	static print() {
		console.log('Hello, World!');
	}

	print() {
		console.log(this.message)
	} 
}